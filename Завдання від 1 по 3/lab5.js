class Menu {
    constructor(sView,s2,s3,s4,textik){
        this.idView=sView;
        this.id2=s2;
        this.id3=s3;
        this.id4=s4;
        this.textik=textik;
    }
    handleEvent(event) {
      switch(event.type) {
        case 'click':
          this.idView.innerHTML = this.textik.innerHTML;
          this.id2.innerHTML = "";
          this.id3.innerHTML = "";
          this.id4.innerHTML = "";
          break;
        case 'mouseover':
            this.idView.innerHTML = this.textik.innerHTML;
            this.id2.innerHTML = "";
            this.id3.innerHTML = "";
            this.id4.innerHTML = "";
            break;
      }
    }
  }

let text1 = new Menu(two1,two2,two3,two4,three1);
let text2 = new Menu(two2,two1,two3,two4,three2);
let text3 = new Menu(two3,two1,two2,two4,three3);
let text4 = new Menu(two4,two1,two2,two3,three4);

one1.addEventListener('click', text1);
one2.addEventListener('click', text2);
one3.addEventListener('click', text3);
one4.addEventListener('click', text4);



one1.addEventListener('mouseover', text1);
one2.addEventListener('mouseover', text2);
one3.addEventListener('mouseover', text3);
one4.addEventListener('mouseover', text4);

const container = document.querySelector('.container');
const containerAddButton = document.querySelector('.container-add-button');

function generatecolor(){
    const hex = Math.floor(Math.random() * 0xffffff).toString(16);
    return `#${hex}`;
}

containerAddButton.addEventListener('click', () => {
    if (container.children.length === 9){
        return;
    }

    const block = document.createElement('div');
    block.style.content = 'dd';
    block.style.border = '1px solid black';
    block.style.backgroundColor = generatecolor();
    block.onclick = () => {
        block.parentElement.removeChild(block);
    };
    container.appendChild(block);
})

window.addEventListener("load", function() {
    let text = document.getElementById("text");
    let red = document.querySelector("#colors>div:nth-child(1)");
    let black = document.querySelector("#colors>div:nth-child(2)");
    let purple = document.querySelector("#colors>div:nth-child(3)");
    let green = document.querySelector("#colors>div:nth-child(4)");
    let yellow = document.querySelector("#colors>div:nth-child(5)");
    let bold = document.querySelector("#font>div:nth-child(1)");
    let cursive = document.querySelector("#font>div:nth-child(2)");
    let underline = document.querySelector("#font>div:nth-child(3)");
    let size = document.getElementById("inp");
    let font = document.querySelector("select");

    red.onclick = function(){
        text.style.color = "#FF0000FF";
    }
    black.onclick = function(){
        text.style.color = "#000000FF";
    }
    purple.onclick = function(){
        text.style.color = "#B404AEFF";
    }
    green.onclick = function(){
        text.style.color = "#04B404FF";
    }
    yellow.onclick = function(){
        text.style.color = "#FFFF00FF";
    }

    bold.onclick = function(){
        if (text.style.fontWeight === "bold"){
            text.style.fontWeight = "normal"
        }
        else{
            text.style.fontWeight = "bold";
        }
    }
    cursive.onclick = function(){
        if (text.style.fontStyle === "italic"){
            text.style.fontStyle = "normal"
        }
        else{
            text.style.fontStyle = "italic";
        }
    }
    underline.onclick = function(){
        if (text.style.textDecoration === "underline"){
            text.style.textDecoration = "none"
        }
        else{
            text.style.textDecoration = "underline";
        }
    }

    size.onchange = function(){
        let x = size.value + "px";
        text.style.fontSize = x;
    }

    font.onchange = function(){
        let x = font.options[font.selectedIndex].text;
        text.style.fontFamily = x;
    }
})