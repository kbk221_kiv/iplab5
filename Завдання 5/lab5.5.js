function getRandomInt(min, max){
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

var a = getRandomInt(1, 10);
var b = getRandomInt(1, 10);
var c = parseInt(a * b);
task.innerHTML = a + ' * ' + b;

form.next.addEventListener('click', function(){
  location.reload();
})

form.pr.addEventListener('click', function(){
  var d = ' = ' + c;
  task.innerHTML += d;
  answer.innerHTML  = 'Ваша відповідь: ' + form.answer.value;
  if(form.answer.value == c){
    result.innerHTML = 'Вірна';
    result.style.color = 'green';
  }else{
    result.innerHTML = 'Не вірно';
    result.style.color = 'red';
  }
  form.answer.value = '';
})